let bodyParser = require('body-parser')
let cors = require('cors')

var express = require('express');
var mysql = require("mysql");
var app = express();

let Principal = require('./controller/PrincipalController')
let Seguridad = require('./controller/SeguridadController')
 
let config = require('./config')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(cors())

app.use(function(req, res, next) {
	res.setHeader('Access-Control-Allow-Origin', '*')
	res.setHeader('Access-Control-Allow-Methods', 'POST,GET')
	res.setHeader('Content-Type', 'application/json')
	next()
});
 
let rutas = express.Router()
rutas.post('/', function (req, res, next) {
    res.send({
		'Mensaje': 'Bienvenido a la API REST!'
	})
});

rutas.route("/sp").post(Principal.prueba)
rutas.route("/Autentica").post(Principal.autentica)
rutas.route("/Enlace").post(Seguridad.protegeRuta, Principal.enlace)
rutas.route("/ConsultaSku").post(Seguridad.protegeRuta, Principal.consultaSku)
rutas.route("/ConsultaRfid").post(Seguridad.protegeRuta, Principal.consultaRfid)
rutas.route("/Desenlace").post(Seguridad.protegeRuta, Principal.desenlazar)
rutas.route("/Sucursales").post(Seguridad.protegeRuta, Principal.sucursales)
rutas.route("/ItemBusqueda").post(Seguridad.protegeRuta, Principal.itemBusqueda)
rutas.route("/GeneraInventario").post( Principal.generaInventario)
rutas.route("/ItemsLeidos").post( Principal.itemsLeidos)
rutas.route("/GeneraMovimiento").post(Principal.generaMovimiento)
rutas.route("/ItemsMovimiento").post(Principal.itemsMovimiento)
rutas.route("/AgregarObservaciones").post(Principal.agregaObservacionMovimiento)



app.use(rutas)
app.listen(config.puerto, function () {
    console.log('Servicio corriendo en http://localhost:'+config.puerto);
});
