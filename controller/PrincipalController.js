let Seguridad = require('./SeguridadController')
let mysql = require("mysql")
let config = require("../config")

let connection = mysql.createConnection({
    host: config.dbconexion.host,
    user: config.dbconexion.user,
    password: config.dbconexion.pass,
    database: config.dbconexion.dbname
});


exports.prueba = (req, res) => {
	if(!connection.connect){
        connection.connect();
    } 
    connection.query('CALL getAllUser(?)',req.body.user,function (err, rows, fields) {
        if (err) {
			res.status(400).send(err);
        }
        res.status(200).send(rows[0]);
    });
}

exports.autentica = (req, res) => {
	if(!connection.connect){
        connection.connect();
    }

    const {IMEI, CLAVEUSER, EMAIL, PLATAFORMA} = req.body; 
    connection.query('CALL login(?,?,?,?)',[IMEI, CLAVEUSER, EMAIL,PLATAFORMA],function (err, rows, fields) {
        if (err) {
			res.status(400).send(err);
        }else{
           
            Seguridad.formatoRespuesta(1, 200, {token:Seguridad.generarToken(rows[0])}, 1, res);
            //res.status(200).send({token : Seguridad.generarToken(rows), "Respuesta: ":"correcta"});
        }
    });
}

exports.enlace = (req, res) =>{
    if(!connection.connect){
        connection.connect();
    }
  
    const {skuIn, rfidIn,idSucursal} = req.body; 
    connection.query('CALL enlazar(?,?,?)',[skuIn, rfidIn,idSucursal],function (err, rows, fields) {
        if (err) {
			res.status(400).send(err);
        }else{
            
            Seguridad.formatoRespuesta(1, 200, rows[0], 1, res);
        }
    });

}

exports.consultaSku = (req, res) =>{
    if(!connection.connect){
        connection.connect();
    }
   
    const {skuIn} = req.body; 
    connection.query('CALL consultaSKU(?)',[skuIn],function (err, rows, fields) {
        if (err) {
			res.status(400).send(err);
        }else{
            
            Seguridad.formatoRespuesta(1, 200, rows[0], 1, res);
        }
    });

}

exports.consultaRfid = (req, res) =>{
    if(!connection.connect){
        connection.connect();
    }
    
    const {rfidIn} = req.body; 
    connection.query('CALL consultaRFID(?)',[rfidIn],function (err, rows, fields) {
        if (err) {
			res.status(400).send(err);
        }else{
            
            Seguridad.formatoRespuesta(1, 200, rows[0], 1, res);
        }
    });

}

exports.desenlazar = (req, res) =>{
    if(!connection.connect){
        connection.connect();
    }
  
    const {rfidIn} = req.body; 
    connection.query('CALL desenlace(?)',[rfidIn],function (err, rows, fields) {
        if (err) {
			res.status(400).send(err);
        }else{
     
            Seguridad.formatoRespuesta(1, 200, rows[0], 1, res);
        }
    });

}

exports.sucursales = (req, res) =>{
    if(!connection.connect){
        connection.connect();
    }

    connection.query('CALL sucursales()',function (err, rows, fields) {
        if (err) {
			res.status(400).send(err);
        }else{
            Seguridad.formatoRespuesta(1, 200, rows[0], 1, res);
        }
    });

}

exports.itemBusqueda = (req, res) =>{
    if(!connection.connect){
        connection.connect();
    }
    const {skuIn} = req.body; 
    connection.query('CALL busqueda(?)',[skuIn],function (err, rows, fields) {
        if (err) {
			res.status(400).send(err);
        }else{
     
            Seguridad.formatoRespuesta(1, 200, rows[0], 1, res);
        }
    });

}

exports.itemsLeidos = (req, res) =>{
    if(!connection.connect){
        connection.connect();
    }
    console.log(req.body)
    const {id_inventario} = req.body; 
    connection.query('CALL itemsLeidos(?)',[id_inventario],function (err, rows, fields) {
        if (err) {
            console.log(err)
			res.status(400).send(err);
        }else{
            console.log(rows)
            Seguridad.formatoRespuesta(1, 200, rows[0], 1, res);
        }
    });

}

exports.generaInventario = async (req, res)  =>{
    if(!connection.connect){
        connection.connect();
    }
    await insertaInventario(req);
    const {usuarioIn,fecha,hora,tipoUsuarioIN,sucursal,empreIn,cantidad} = req.body;
    
    connection.query('CALL insertaInventario(?,?,?,?,?,?,?)',[usuarioIn,fecha,hora,tipoUsuarioIN,sucursal,empreIn,cantidad],function (err, rows, fields) {
        if (err) {
            
            res.status(400).send(err);
        }else{
            Seguridad.formatoRespuesta(1, 200, rows[0], 1, res);
        }
    });    
}



const insertaInventario = (req) =>{
    let array = req.body.cadena.split(",")
    //console.log(array)
    const {usuarioIn,fecha,hora,tipoUsuarioIN,sucursal,empreIn,cantidad} = req.body;
    let datos;
    let id_sku ="";
    let id_rfid ="";
    let i=0;
    
    do {
        connection.query('SELECT * FROM RFID WHERE RFID = (?)',array[i],function (err, rows, fields) {
            if (err) {
                console.log(err)
            }else{
                console.log(rows)
                if(rows[0] != undefined && rows[0] != []){
                    var {SKUS_id_sku, id_rfid} =rows[0];
                    id_sku = SKUS_id_sku;
                    
                    if(id_sku != ""){
                        connection.query('SELECT * FROM SKUS, RFID WHERE SKUS.id_sku = (?) AND RFID.id_rfid = (?)',[id_sku, id_rfid],function (err, rows, fields) {
                            if (err) {
                               console.log(err)
                            }else{
                               // console.log("-------")
                                console.log(rows)
                                const {id_sku,descrip_sku,id_rfid}= rows[0];
                                connection.query('CALL insertaItemsInventario(?,?,?)',[descrip_sku,id_rfid,id_sku],function (err, rows, fields) {
                                    if (err) {
                                        console.log(err)
                                    }else{
                                        
                                       // console.log("-------")
                                        console.log(rows)
                                                                    
                                    }
                                });                            
                            }
                        });
                    }
                }
            }
        });
        i++
       
    }while ( i < array.length);

}



exports.generaMovimiento = async (req, res)  =>{
    if(!connection.connect){
        connection.connect();
    }
    await insertaMovimiento(req);
    const {fecha,hora,id_usuario,id_sucursal} = req.body;
    
    connection.query('CALL insertaMovimiento(?,?,?,?)',[fecha,hora,id_usuario,id_sucursal],function (err, rows, fields) {
        if (err) {
            
            res.status(400).send(err);
        }else{
            Seguridad.formatoRespuesta(1, 200, rows[0], 1, res);
        }
    });    
}

const insertaMovimiento = (req) =>{
    let array = req.body.cadena.split(",")
    //console.log(array)
  

    let i=0;
    
    do {
        connection.query('SELECT * FROM RFID WHERE RFID = (?)',array[i],function (err, rows, fields) {
            if (err) {
                console.log(err)
            }else{
                console.log(rows)
                if(rows[0] != undefined && rows[0] != []){
                    const {SKUS_id_sku, id_rfid} =rows[0];
                    id_sku = SKUS_id_sku;
                    console.log(rows)
                    if(id_sku != ""){
                        connection.query('SELECT * FROM SKUS, RFID WHERE SKUS.id_sku = (?) AND RFID.id_rfid = (?)',[id_sku, id_rfid],function (err, rows, fields) {
                            if (err) {
                               console.log(err)
                            }else{
                               // console.log("-------")
                                console.log(rows)
                                const {id_sku,descrip_sku,id_rfid}= rows[0];
                                const {id_origen, id_destino} = req.body;
                                connection.query('CALL insertaItemsMovimiento(?,?,?,?,?)',[descrip_sku,id_rfid,id_sku, id_origen, id_destino],function (err, rows, fields) {
                                    if (err) {
                                        console.log(err)
                                    }else{
                                        
                                       // console.log("-------")
                                        console.log(rows)
                                                                    
                                    }
                                });                            
                            }
                        });
                    }
                }
            }
        });
        i++
       
    }while ( i < array.length);

}


exports.itemsMovimiento = (req, res) =>{
    if(!connection.connect){
        connection.connect();
    }
    console.log("itemsMovimiento: "+req.body)
    const {id_movimiento} = req.body; 
    connection.query('CALL itemsContenidoMovimiento(?)',[id_movimiento],function (err, rows, fields) {
        if (err) {
            console.log("resp:"+ err)
			res.status(400).send(err);
        }else{
            console.log("resp:"+ rows)
            Seguridad.formatoRespuesta(1, 200, rows[0], 1, res);
        }
    });

}



exports.agregaObservacionMovimiento = (req, res) =>{
    if(!connection.connect){
        connection.connect();
    }
    
    const {observaciones,id_movimiento} = req.body; 
    connection.query('CALL agregaObservacionMovimiento(?,?)',[observaciones,id_movimiento],function (err, rows, fields) {
        if (err) {
            
			res.status(400).send(err);
        }else{
           
            Seguridad.formatoRespuesta(1, 200, rows[0], 1, res);
        }
    });

}
