let date = new Date();

let nodemailer = require("nodemailer")
let fs = require('fs')
const pathExcel = 'C:\\reporteinventario.xlsx'
const nombreExcel = 'inventario '+date+'.xlsx'
let Seguridad = require('./SeguridadController')
let config = require('../config')

var excel = require ( 'excel4node' );


var retornaObjeto = x => ( Object.keys(x.params).length === 0 ? x.body : x.params)

const enviarCorreo =  async function(correoTo, correoCC, asunto) {
	let transporter = nodemailer.createTransport({
		service: 'gmail',
		auth: {
			user: config.correo.usuario
			, pass: config.correo.pass
		}
    });
    
	let info = await transporter.sendMail({
        from: config.correo.usuario,
		to: correoTo, // list of receivers
        subject: asunto, // Subject line
        cc: correoCC,
		html: `<p>Se adjunta Excel con inventario </p>`, // html body
		attachments: [{filename: nombreExcel, path:pathExcel}]
	});
}
const borrarArchivoInventario = () => {
    if(fs.existsSync(pathExcel)) {
        fs.unlink(pathExcel, (err)=>{ 
            if(err){
                throw err;

            }else{
                console.log("archivo eliminado");
            }
        })
    }
}



const guardarExcel = (datos) => {
    var workbook =  new excel.Workbook ();

    var worksheet = workbook.addWorksheet ( 'ReporteEspecifico' );
    var worksheet2 = workbook.addWorksheet ( 'ReporteGeneral' );
    

	console.log("guarda excel")
   
    
    borrarArchivoInventario();
    workbook.write ( 'reporteinventario.xlsx' );
    enviarCorreo(correoTo, correoCC, asunto);
    //guardaArchivoInventario(data);
}